<?php 
error_reporting(E_ALL ^ E_NOTICE);
require_once('conexion.php');
?>
<?php
$max=12;
$pag=0;
if(isset($_GET[pag]) && $_GET[pag] <>""){
$pag=$_GET[pag];
}
$inicio=$pag * $max;
$query=" SELECT * FROM productos ORDER BY fecha DESC";
$query_limit= $query ." LIMIT $inicio,$max";
$resource = $conn->query($query_limit);
if (isset($_GET[total])) {
$total = $_GET[total];
} else {
$resource_total = $conn -> query($query);
$total = $resource_total->num_rows;
}
$total_pag = ceil($total/$max)-1;
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <?php include("head.php");?>
  </head>
  <body>
   
    <!-- header -->
    <?php include("header1.php");?><!-- fin header -->
            
    <!-- Menu Principal -->
    <?php include("menu.php");?>    
    <!-- End Menu Principal -->
    
    <!-- Encabezado -->
    <div class="product-big-title-area padd">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Nosotros</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Fin Encabezado -->
    
    
    <div class="promo-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row padd-y-b-10">
            <div class="product-big-title-area padd">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-bit-title text-center">
                                <h3>Bienvenidos a Organizadora de Eventos Mary</h3>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <article class="col-md-6">
                    <h4><i class="fa fa-truck"></i> Reciba sus Productos en la Comodidad de su Hogar</h4>
                    <p>Nuestro servicio de alquiler a domicilio a través de Organizadora de Eventos Mary le proporciona la seguridad y calidad en sus alquileres desde la comodidad de su hogar, oficina o lugar de trabajo, usted puede hacer su pedido via internet con nuestro moderno sistema de "carrito de compras" incorporado a este sitioweb.</p>
                    <p>Nuestra empresa ha sido creada para llevar a su hogar un servicio integral e innovador en el alquiler a domicilio. Organizadora de Eventos Mary, un concepto de organizacion moderna.</p>
                    <p>Para ello, buscamos y seleccionamos los mejores productos, para que disfrute de los eventos mas comodos y organizados.</p>
                </article>
                <article class="col-md-6">
                    <h4><i class="fa fa-truck"></i> Reparto a Domicilio (Mesas, sillas, toldos.)</h4>
                    <p>La comodidad que presenta el servicio de alquiler a domicilio es extraordinario ya que las ventajas comparativas son muchas</p> 
                    <ul class="list-unstyled">
                        <li>Entrega a domicilio de los productos de primera calidad.</li>
                        <li>Los mismos precios de las demas tienda.</li>
                        <li>Mejor relación precio y calidad.</li>
                        <li>Economía en sus compras en tiempo y dinero.</li>
                        <li>Más tiempo para dedicar a su familia.</li>
                        <li>Compra fácil y rápida, con gran comodidad para el cliente.</li>
                        <li>Sin costos de envío (Sobre 300Bs. en alquiler)</li>
                        <li>Descubres las ventajas de comprar desde la comodidad de su hogar.</li>
                    </ul>   
                </article>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo">
                        <i class="fa fa-thumbs-o-up"></i>
                        <p>Política de devolución al instante</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo">
                        <i class="fa fa-truck"></i>
                        <p>Despacho Gratis Sobre Los 300Bs.</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo">
                        <i class="fa fa-lock"></i>
                        <p>Pago De la Forma mas Accesible</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo">
                            <i class="fa fa-cart-plus"></i>
                            <p>Compre directo desde su Hogar</p>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End promo area -->
    
    <div class="product-widget-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <h2 class="section-title">Últimos Productos</h2>
                <?php  while ($row = $resource->fetch_assoc()){?>
                <div class="col-md-4">
                    <div class="single-product-widget">
                        <div class="single-wid-product">
                            <a href="producto.php?id=<?php echo $row[id]?>"><img src="img/<?php echo $row[codigo]?>.jpg" alt="" class="product-thumb img-thumbnail"></a>
                            <h2><a href="producto.php?id=<?php echo $row[id]?>"><?php echo $row[nombre]?></a></h2>
                            <div class="product-wid-price">
                               <ins>$ <?php echo $row[precio]?> <?php echo $row[unidad]?></ins> Antes Bs.<del><?php echo $row[precio]+($row[precio]*0.4)?></del>
                            </div>                            
                        </div>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
        <center><a href="tienda.php" class="btn btn-success">Ver todos los Productos</a></center>
    </div> <!-- End product widget area -->
    
    <!-- Footer -->
    <?php include("footer.php");?><!-- End Footer -->
    <!-- JS -->
    <?php include("js.php");?><!-- End JS -->
  </body>
</html>