<?php 
error_reporting(E_ALL ^ E_NOTICE);
require_once("conexion.php")?>
<?php 
if(!$_SESSION[user_id]){
$_SESSION[volver]=$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING'];
header("Location: login.php");
}
?>
<?php	
      $q="SELECT * FROM compras WHERE 1 AND cliente='$_SESSION[user_id]' ORDER BY fecha DESC";
      $r = $conn->query($q); 
      $t = $r->num_rows;
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <?php include("head.php");?>
    <style>
    .descuento{
        display: none;
    }  
    </style>
  </head>
  <body>
    <!-- header -->
    <?php include("header1.php");?><!-- fin header --> 

    <!-- Menu Principal -->
    <?php include("menu.php");?>    
    <!-- End Menu Principal -->
    
    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Gracias Por Su Preferencia</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
            
                <div class="col-md-12">
                    <div class="product-content-right">
                        <div class="woocommerce">

                            <center><h2>COnfirme su Alquiler</h2></center>
                            <div class="table-responsive col-xs-12">
                                    <table cellspacing="0" class="shop_table cart">
                                        <thead>
                                            <tr>
                                                <th class="product-thumbnail"><i class="fa fa-file-image-o" aria-hidden="true"></i> Foto</th>
                                                <th class="product-name"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Producto</th>
                                                <th class="product-price"><i class="fa fa-usd" aria-hidden="true"></i> Precio</th>
                                                <th class="product-quantity">Cantidad</th>
                                                <th class="product-subtotal"><i class="fa fa-usd" aria-hidden="true"></i> Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php while ($row = $r->fetch_assoc()){?>
                                            <tr class="cart_item wow fadeIn">
                                                <td class="product-thumbnail">
                                                  <img width="145" height="145" alt="<?php echo $row[nombre]?>" class="shop_thumbnail" src="img/<?php echo $row[codigo]?>.jpg">
                                                </td>

                                                <td class="product-name">
                                                    <?php echo $row[nombre]?>
                                                </td>

                                                <td class="product-price">
                                                    <span class="amount">Bs.<?php echo number_format($precio=$row[precio], 0, ',', '.');?>
                                                    </span> 
                                                </td>

                                                <td class="product-quantity">
                                                    <div class="quantity buttons_added">
                                                        <?php echo $cantidad=$row[cantidad]?>
                                                    </div>
                                                </td>

                                                <td class="product-subtotal">
                                                    <span class="amount">Bs.<?php echo number_format($sub=$precio*$cantidad); $subtotal+=$sub?></span> 
                                                </td>
                                            </tr>
                                            <?php }?>  
                                        </tbody>
                                    </table>
                                </div>
                            <div class="cart_totals col-xs-12 wow fadeIn">
                                    <table cellspacing="0">
                                        <tbody>
                                        
                                            <form action="CreateCharge.php" method="post" id="payment-form">
                                            <tr>
                                                <div class="form-row">
                                                    <Center><label for="card-element">Tarjeta de Credito o Debito</label></Center>
                                                    <div id="card-element">
                                                    <!-- A Stripe Element will be inserted here. -->
                                                    </div>
                                                    
                                                    <!-- Used to display form errors. -->
                                                    <div id="card-errors" role="alert"></div>
                                                </div>
                                            </tr>
                                            <tr>
                                                <center><input type="submit" name="button" id="button" value="Pagar" class="btn btn-success"></center>
                                            </tr>
                                            </form>
                                         
                                            <tr class="shipping">
                                                <th>Costo De Envío</th>
                                                <td>Bs.<?php 
                                                    if($subtotal > 300){
                                                    $envio=0;
                                                    } elseif($subtotal >150){
                                                    $envio=100;
                                                    } else {
                                                    $envio=80;
                                                    }
                                                    echo number_format($envio,0, ',', '.');?>
                                               </td>
                                            </tr>

                                             <tr id="descuento" <?php if($subtotal <= 300) : ?>class="descuento"<?php endif;?>>
                                                  <td class="success">Descuento 10%</td>
                                                  <td class="success">-Bs.<?php 
                                                        if($subtotal > 300){
                                                            echo number_format($descuento=($subtotal) *0.10, 0, ',', '.');                
                                                        }else{
                                                           $descuento=0; 
                                                        }
                                                ?></td>
                                            </tr>
                                            <tr>
                                                <td>Subtotal</td>
                                                <td>Bs.<?php echo number_format($subtotal=($subtotal+$envio)-$descuento, 0, ',', '.');?></td>
                                            </tr>

                                            <tr class="order-total">
                                                <th>Total Pedido</th>
                                                <td><strong><span class="amount">Bs.<?php echo number_format($total = $subtotal, 0, ',', '.');?></span></strong> </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                        </div>                       
                    </div>                    
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    <?php include("footer.php");?><!-- End Footer -->   
    <!-- JS -->
    <?php include("js.php");?><!-- End JS -->
    <script>
      // Create a Stripe client.
      var stripe = Stripe('pk_test_qbc8Z30hTdaurCYH64VzRnau');

      // Create an instance of Elements.
      var elements = stripe.elements();

      // Custom styling can be passed to options when creating an Element.
      // (Note that this demo uses a wider set of styles than the guide below.)
      var style = {
        base: {
          color: '#32325d',
          lineHeight: '18px',
          fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
          fontSmoothing: 'antialiased',
          fontSize: '16px',
          '::placeholder': {
            color: '#aab7c4'
          }
        },
        invalid: {
          color: '#fa755a',
          iconColor: '#fa755a'
        }
      };

      // Create an instance of the card Element.
      var card = elements.create('card', {style: style});

      // Add an instance of the card Element into the `card-element` <div>.
      card.mount('#card-element');

      // Handle real-time validation errors from the card Element.
      card.addEventListener('change', function(event) {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
          displayError.textContent = event.error.message;
        } else {
          displayError.textContent = '';
        }
      });

      // Handle form submission.
      var form = document.getElementById('payment-form');
      form.addEventListener('submit', function(event) {
        event.preventDefault();

        stripe.createToken(card).then(function(result) {
          if (result.error) {
            // Inform the user if there was an error.
            var errorElement = document.getElementById('card-errors');
            errorElement.textContent = result.error.message;
          } else {
            // Send the token to your server.
            stripeTokenHandler(result.token);
          }
        });
      });

      function stripeTokenHandler(token) {
        // Insert the token ID into the form so it gets submitted to the server
        var form = document.getElementById('payment-form');
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);

        // Submit the form
        form.submit();
      }
    </script>
  </body>
</html>